#include <iostream>

class PrimeGenerator
{
private:
	static int s_currentPrime;

public:
	static int GetNextPrime()
	{
		for(int i = s_currentPrime + 1;; i++)
		{
			int j;
			for(j = 2; j<i; j++)
			{
				if(i%j == 0)
					break;
			}

			if(j == i)
			{
				s_currentPrime = i;
				return s_currentPrime;
				break;
			}
		}
	}

	static int GetCurrentPrime()
	{
		return s_currentPrime;
	}
};
int PrimeGenerator::s_currentPrime = 1;

int main()
{
	size_t i = 0;
	while(true)
	{
		std::cout << i << ": " << PrimeGenerator::GetNextPrime() << std::endl;
		i++;
		getc(stdin);
	}
}